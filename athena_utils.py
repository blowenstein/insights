import boto3
import pandas as pd
import time
from datetime import datetime


# queries athena
def query_athena(query, db):
    print(f'Running against db: {db} query:\n {query}')
    client = boto3.client('athena')
    output_location = 's3://stackline-data/zeppelin-tempdir'
    response = client.start_query_execution(
        QueryString=query,
        QueryExecutionContext={
            'Database': db
        },
        ResultConfiguration={
            'OutputLocation': output_location
        },
        WorkGroup='insights'
    )
    query_id = response['QueryExecutionId']
    query_status = None
    start_time = datetime.now()
    while query_status != 'SUCCEEDED':
        query_status = \
        client.get_query_execution(QueryExecutionId=response['QueryExecutionId'])['QueryExecution']['Status']['State']
        now = datetime.now()
        print(f'Query: {query_id} Status: {query_status} after {(now - start_time).seconds} seconds')
        if query_status == 'SUCCEEDED':
            break
        if query_status == 'FAILED':
            errored_status = client.get_query_execution(QueryExecutionId=response['QueryExecutionId'])
            RuntimeError(f'Query Failed {errored_status}')
        time.sleep(2)
    end_time = datetime.now()
    bytes_scanned = client.get_query_execution(QueryExecutionId=response['QueryExecutionId'])['QueryExecution']['Statistics']['DataScannedInBytes']
    duration = (end_time - start_time).seconds
    file_path = f'{output_location}/{query_id}.csv'
    print(f'Query took {duration} seconds and scanned {bytes_scanned} bytes. Reading it from {file_path}')

    s3_client = boto3.client('s3')
    obj = s3_client.get_object(Bucket='stackline-data', Key=f'zeppelin-tempdir/{query_id}.csv')
    return pd.read_csv(obj['Body'])
