import gzip
import logging
import os
import uuid
from datetime import datetime, timedelta
import boto3
import botocore
import requests
from batch import run_batch_job
from airflow import DAG
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.contrib.sensors.emr_step_sensor import EmrStepSensor
from airflow.hooks.base_hook import BaseHook
from airflow.operators.python_operator import PythonOperator
from init_helper import init_dag


SLACK_CONN_ID = 'slack_insights'


def task_fail_slack_alert(context):
    slack_webhook_token = BaseHook.get_connection(SLACK_CONN_ID).password
    slack_msg = """
			:red_circle: JOB FAILED.
			*Task*: {task} 
			*Execution Time*: {exec_date}
			*Airflow Log*: {log_url} 
			""".format(
        task=context.get('task_instance').task_id,
        ti=context.get('task_instance'),
        exec_date=context.get('execution_date'),
        log_url=context.get('task_instance').log_url
    )
    failed_alert = SlackWebhookOperator(
        task_id='slack_fail',
        http_conn_id=SLACK_CONN_ID,
        webhook_token=slack_webhook_token,
        message=slack_msg,
        username='airflow',
        dag=dag)
    return failed_alert.execute(context=context)


def task_success_slack_alert(context):
    slack_webhook_token = BaseHook.get_connection(SLACK_CONN_ID).password
    slack_msg = """
			:white_check_mark: Job Succeeded.
			*Task*: {task} 
			*Execution Time*: {exec_date}
			*Airflow Log*: {log_url}
			""".format(
        task=context.get('task_instance').task_id,
        ti=context.get('task_instance'),
        exec_date=context.get('execution_date'),
        log_url=context.get('task_instance').log_url,
    )
    success_alert = SlackWebhookOperator(
        task_id='slack_success',
        http_conn_id=SLACK_CONN_ID,
        webhook_token=slack_webhook_token,
        message=slack_msg,
        username='airflow',
        dag=dag)
    return success_alert.execute(context=context)


start_date = datetime(2018, 10, 31)
default_args = {
    'owner': 'airflow',
    'start_date': start_date,
    'email': ['blowenstein@stackline.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'on_success_callback': task_success_slack_alert,
    'on_failure_callback': task_fail_slack_alert,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
    'provide_context': True
}


dag = DAG('insights_brand_1p_3p_dag', schedule_interval=None, catchup=False, default_args=default_args,
          max_active_runs=5, concurrency=5)


def init_conf(required_params, optional_params, **kwargs):
    print(f'Got the following params - required: {required_params} with optionals: {optional_params}')
    init_dag(required_params, optional_params, **kwargs)


initial_task = PythonOperator(
    task_id='initial_task',
    python_callable=init_conf,
    op_kwargs={
        "required_params": ['beacon_client_id', 'end_week_id'],
        "optional_params": {}
    },
    dag=dag
)

run_crawl = PythonOperator()

# run_batch_job(
#         job_name,
#         job_queue,
#         job_definition,
#         commands,
#         vcpus=0,
#         memory=0,
#         poke_interval=30,

run_brand_report = PythonOperator(
    task_id='run_brand_report',
    python_callable=run_batch_job,
    op_kwargs={
        'job_queue': ''
    }
)

email_task = PythonOperator()

#TODO add a qc step before emailing client

initial_task >> run_crawl >> run_brand_report >> email_task
