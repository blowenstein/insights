from sl_py_utils.db.redshift import SLRedshiftCompute


def get_client_division_data(beacon_client_id):
    with SLRedshiftCompute() as compute:
        result = compute.query_as_df(
            f'''
                SELECT companyname, divisionid, divisionname, atlasbrandsowned, atlasretailerssubscribed, atlascategoriessubscribed
                FROM common.clientcompanydivisions CCD
                     INNER JOIN common.clientcompanies CC ON CC.companyid = CCD.companyid
                WHERE BeaconClientId = {beacon_client_id}
                    AND CC.companyname NOT ILIKE '%stackline%'
                    AND CC.companyname NOT ILIKE '%demo%'
               ''')
    return result


def __strlist_to_list__(input):
    return list(map(lambda x: x.lstrip('[').rstrip(']'), input.split(',')))
