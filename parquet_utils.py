import pandas as pd
import s3fs

fs = s3fs.core.S3FileSystem()

def read_parquet(path):
    all_paths_from_s3 = list(map(lambda x: f's3://{x}', fs.glob(path=path)))
    dfs = []
    for path in all_paths_from_s3:
        print(f'Reading path as parquet: {path}')
        df = pd.read_parquet(path)
        dfs.append(df)
    df = pd.concat(dfs, ignore_index=True)
    print(f'Creating df from {path} length: {len(df)} with schema:\n{df.dtypes} ')
    return df
