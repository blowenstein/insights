from data_collection_utils import get_client_division_data, __strlist_to_list__
import pandas as pd
import s3fs
import functools
import numpy as np
from athena_utils import query_athena
import argparse
import boto3

fs = s3fs.core.S3FileSystem()
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)



def __outer_merge_indices__(l, r):
    print(f'joining 2 dfs {l.columns} + {r.columns}')
    print(f'len l + {len(l)}')
    print(f'len r + {len(r)}')

    return l.join(r, how='outer')


def get_atlas_data_athena(brand_ids, cat_ids, sku_to_brand, min_week, max_week):
    aps_raw = query_athena(
        f'''
            select retailersku, weekid, estimatesales as PlatformSales from atlas.atlasproductsales
            where 
                retailerid = 1 and
                weekid >= {min_week} and
                weekid <= {max_week} and
                retailersku in 
                (   select retailersku 
                    from product.retailerproducts_202010_1583347235
                    where retailerid = 1 and brandid in ({','.join(brand_ids)}) and categoryid in ({','.join(cat_ids)})
                ) 
        ''',
        'atlas'
    ).rename(columns={'PlatformSales': 'Platform Sales'})
    aps_raw = pd.merge(aps_raw, sku_to_brand, how='inner', left_on=['retailersku'], right_on=['retailersku'])
    aps_raw['weekid'] = aps_raw['weekid'].astype(int)
    aps_raw['brandid'] = aps_raw['brandid'].astype(int)
    aps_raw['categoryid'] = aps_raw['categoryid'].astype(int)
    aps_raw['Platform Sales'] = aps_raw['Platform Sales'].astype(float).fillna(0)

    return aps_raw


def get_beacon_data(brand_ids, beacon_client, brand_mapping, min_week):
    print(f'Reading in beacon data for client: {beacon_client} brand_ids: {brand_ids}')
    week_partition_paths = list(map(lambda x: f's3://{x}', fs.glob(
        path=f's3://beacon-publishing/v2/_beaconClientId={beacon_client}/_retailerId=1/_rollupType=sales/')))
    dfs = []
    for week_partition in week_partition_paths:
        if int(week_partition.split('_weekId=')[1]) < int(min_week):
            continue
        part_files = list(map(lambda x: f's3://{x}', fs.glob(path=week_partition + '/')))
        for part in part_files:
            print(f'Reading partfile: {part}')
            df = pd.read_csv(part)
            df = df[['weekId', 'retailerSku', 'retailSales']]
            dfs.append(df)
    raw_df = pd.concat(dfs, ignore_index=True)
    raw_df = raw_df.rename(columns={'retailerSku': 'retailersku'}).rename(columns={'retailSales': '1P Sales'})
    brand_beacon_data = pd.merge(raw_df, brand_mapping, how='inner', left_on=['retailersku'], right_on=['retailersku'])
    beacon_data_filtered = brand_beacon_data[brand_beacon_data['brandid'].isin(brand_ids)]
    return beacon_data_filtered


def read_retailerproducts(brand_ids, retailer_ids):
    raw_df = query_athena(
        f'''
            select retailersku, retailerid, brandname_normalized as Brand, brandid, categoryid, categoryname
            from product.retailerproducts202008_1582372808
            where retailerid = 1 and brandid in ({','.join(brand_ids)})
        ''',
        'atlas'
    )
    df = raw_df.rename(columns={'retailerSku': 'retailersku'})
    df = df[df['brandid'].isin(brand_ids) & df['retailerid'].isin(retailer_ids)]
    df = df[['retailersku', 'brandid', 'Brand', 'categoryid', 'categoryname']]
    return df


def rollup_skus(df, weeks, max_weekid):
    df = df[['Brand', 'retailersku', 'weekid', 'Platform Sales', '1P Sales']]
    df = df.set_index(['retailersku', 'weekid', 'Brand']).sort_index().rolling(weeks).sum()
    df['3P Sales'] = df['Platform Sales'] - df['1P Sales']
    product = [[f'L{weeks} Weeks', f'L{weeks} Weeks', f'L{weeks} Weeks'],
               ['Platform Sales', '1P Sales', '3P Sales']]
    multi_indices = pd.MultiIndex.from_arrays(product, names=['', ''])
    df = df.reset_index()
    df = df[df['weekid'] == max_weekid]
    df = df.drop(columns=['weekid'])
    df = df.set_index(['Brand', 'retailersku'])
    df.columns = multi_indices

    return df


def rollup_brands(df, weeks, max_weekid):
    df = df[['Brand', 'weekid', 'Platform Sales', '1P Sales']]
    df = df.groupby(['Brand', 'weekid']).sum()
    df = df.sort_index().rolling(weeks).sum()
    df['3P Sales'] = df['Platform Sales'] - df['1P Sales']
    product = [[f'L{weeks} Weeks', f'L{weeks} Weeks', f'L{weeks} Weeks'], ['Platform Sales', '1P Sales', '3P Sales']]
    multi_indices = pd.MultiIndex.from_arrays(product, names=['', ''])
    df = df.reset_index()
    df = df[df['weekid'] == max_weekid]
    df = df.drop(columns=['weekid'])
    df = df.drop_duplicates()
    df = df.set_index(['Brand'])
    df.columns = multi_indices

    return df


def rollup_brand_level(joint_df, max_weekid):
    lw = rollup_brands(joint_df, 1, max_weekid)
    four_rollup = rollup_brands(joint_df, 4, max_weekid)
    thirteeen_rollup = rollup_brands(joint_df, 13, max_weekid)
    twentysix_rollup = rollup_brands(joint_df, 26, max_weekid)
    fiftytwo_rollup = rollup_brands(joint_df, 52, max_weekid)

    start_of_year = int(str(max_weekid)[0:4] + '01')
    ytd_rollup = joint_df[['Brand', 'weekid', 'Platform Sales', '1P Sales']]
    ytd_rollup = ytd_rollup[ytd_rollup['weekid'] >= start_of_year]
    ytd_rollup = ytd_rollup.drop(columns=['weekid'])
    ytd_rollup = ytd_rollup.groupby(['Brand']).sum()
    ytd_rollup['3P Sales'] = ytd_rollup['Platform Sales'] - ytd_rollup['1P Sales']
    product = [[f'YTD', f'YTD', f'YTD'], ['Platform Sales', '1P Sales', '3P Sales']]
    multi_indices = pd.MultiIndex.from_arrays(product, names=['', ''])
    ytd_rollup = ytd_rollup.reset_index()
    ytd_rollup = ytd_rollup.drop_duplicates()
    ytd_rollup = ytd_rollup.set_index(['Brand'])
    ytd_rollup.columns = multi_indices

    result = functools.reduce(lambda l, r: __outer_merge_indices__(l, r),
                              [lw, four_rollup, thirteeen_rollup, twentysix_rollup, fiftytwo_rollup, ytd_rollup])
    return result


def rollup_sku_level(joint_df, max_weekid):
    lw = rollup_skus(joint_df, 1, max_weekid)
    print(f'Found {len(lw)} rows in lw')
    four_rollup = rollup_skus(joint_df, 4, max_weekid)
    print(f'Found {len(four_rollup)} rows in 4w')
    thirteeen_rollup = rollup_skus(joint_df, 13, max_weekid)
    print(f'Found {len(thirteeen_rollup)} rows in 13w')
    twentysix_rollup = rollup_skus(joint_df, 26, max_weekid)
    print(f'Found {len(twentysix_rollup)} rows in 26w')
    fiftytwo_rollup = rollup_skus(joint_df, 52, max_weekid)
    print(f'Found {len(fiftytwo_rollup)} rows in 52w')

    start_of_year = int(str(max_weekid)[0:4] + '01')
    ytd_rollup = joint_df[joint_df['weekid'] >= start_of_year]
    ytd_rollup = ytd_rollup[['Brand', 'retailersku', 'Platform Sales', '1P Sales']]
    ytd_rollup = ytd_rollup.groupby(['retailersku', 'Brand']).sum()
    ytd_rollup['3P Sales'] = ytd_rollup['Platform Sales'] - ytd_rollup['1P Sales']
    product = [[f'YTD', f'YTD', f'YTD'], ['Platform Sales', '1P Sales', '3P Sales']]
    multi_indices = pd.MultiIndex.from_arrays(product, names=['', ''])
    ytd_rollup = ytd_rollup.reset_index()
    ytd_rollup = ytd_rollup.set_index(['Brand', 'retailersku'])
    ytd_rollup.columns = multi_indices

    print(f'Found {len(ytd_rollup)} rows in Ytd')

    result = functools.reduce(lambda l, r: __outer_merge_indices__(l, r),
                              [lw, four_rollup, thirteeen_rollup, twentysix_rollup, fiftytwo_rollup, ytd_rollup])
    print(f'Found {len(result)} rows in all rollups merged')
    result = result.reset_index(col_level=1, col_fill='meta')
    result['type'] = np.where(result['L52 Weeks']['1P Sales'] == '0', '3P Only', 'Mixed 1P/3P')
    result = result.set_index('type').reset_index(col_level=1, col_fill='meta')
    result = result[['meta', 'type', 'L1 Weeks', 'L4 Weeks', 'L13 Weeks', 'L26 Weeks', 'L52 Weeks', 'YTD']]
    result = result.rename(columns={'meta': ''})
    result = result.sort_values(by=[('', 'Brand'), ('L1 Weeks', 'Platform Sales')], ascending=[True, False])
    result = result[
        [
            ('', 'Brand'), ('', 'retailersku'), ('', 'type'),
            ('L1 Weeks', 'Platform Sales'), ('L1 Weeks', '1P Sales'), ('L1 Weeks', '3P Sales'),
            ('L4 Weeks', 'Platform Sales'), ('L4 Weeks', '1P Sales'), ('L4 Weeks', '3P Sales'),
            ('L13 Weeks', 'Platform Sales'), ('L13 Weeks', '1P Sales'), ('L13 Weeks', '3P Sales'),
            ('L26 Weeks', 'Platform Sales'), ('L26 Weeks', '1P Sales'), ('L26 Weeks', '3P Sales'),
            ('L52 Weeks', 'Platform Sales'), ('L52 Weeks', '1P Sales'), ('L52 Weeks', '3P Sales'),
            ('YTD', 'Platform Sales'), ('YTD', '1P Sales'), ('YTD', '3P Sales')
        ]
    ]
    result = result.reindex()
    return result

def write_brand_level(df, writer):
    df.to_excel(writer, startrow=1, header=False, sheet_name='BrandSummary')
    # Get the xlsxwriter workbook and worksheet objects.
    workbook = writer.book
    worksheet = writer.sheets['BrandSummary']
    merge_format = workbook.add_format({
        'bold': True,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
    })
    header_format = workbook.add_format({
        'bold': True,
        'valign': 'top',
        'border': 1}
    )
    currency_format = workbook.add_format({'num_format': '[$$-409]#,##0.00'})

    worksheet.write(f'A2', 'Brand', header_format)
    worksheet.merge_range('B1:D1', 'LW', merge_format)
    worksheet.write(f'B2', 'Platform Sales', header_format)
    worksheet.write(f'C2', '1P Sales', header_format)
    worksheet.write(f'D2', '3P Sales', header_format)
    worksheet.merge_range('E1:G1', '4W', merge_format)
    worksheet.write(f'E2', 'Platform Sales', header_format)
    worksheet.write(f'F2', '1P Sales', header_format)
    worksheet.write(f'G2', '3P Sales', header_format)
    worksheet.merge_range('H1:J1', '13W', merge_format)
    worksheet.write(f'H2', 'Platform Sales', header_format)
    worksheet.write(f'I2', '1P Sales', header_format)
    worksheet.write(f'J2', '3P Sales', header_format)
    worksheet.merge_range('K1:M1', '26W', merge_format)
    worksheet.write(f'K2', 'Platform Sales', header_format)
    worksheet.write(f'L2', '1P Sales', header_format)
    worksheet.write(f'M2', '3P Sales', header_format)
    worksheet.merge_range('N1:P1', '52W', merge_format)
    worksheet.write(f'N2', 'Platform Sales', header_format)
    worksheet.write(f'O2', '1P Sales', header_format)
    worksheet.write(f'P2', '3P Sales', header_format)
    worksheet.merge_range('Q1:S1', 'YTD', merge_format)
    worksheet.write(f'Q2', 'Platform Sales', header_format)
    worksheet.write(f'R2', '1P Sales', header_format)
    worksheet.write(f'S2', '3P Sales', header_format)
    worksheet.set_column('B:S', 11, currency_format)
    worksheet.set_column('A:A', 27, currency_format)

    print(f'Done writing to BrandSummary')


def write_asin_level(sku_rollup, writer):
    multi = pd.MultiIndex.from_frame(sku_rollup[['']], names=['Brand', 'retailersku', 'type'])
    sku_rollup.index = multi
    sku_rollup = sku_rollup.drop(columns=[''])
    sku_rollup.to_excel(writer, startrow=1, header=False, index_label=None, sheet_name='ASIN Detail')

    # Get the xlsxwriter workbook and worksheet objects.
    workbook = writer.book
    worksheet = writer.sheets['ASIN Detail']
    merge_format = workbook.add_format({
        'bold': True,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
    })
    header_format = workbook.add_format({
        'bold': True,
        'valign': 'top',
        'border': 1}
    )
    currency_format = workbook.add_format({'num_format': '[$$-409]#,##0.00'})

    worksheet.write(f'A2', 'Brand', header_format)
    worksheet.write(f'B2', 'RetailerSku', header_format)
    worksheet.write(f'C2', 'Type', header_format)
    worksheet.merge_range('D1:F1', 'LW', merge_format)
    worksheet.write(f'D2', 'Platform Sales', header_format)
    worksheet.write(f'E2', '1P Sales', header_format)
    worksheet.write(f'F2', '3P Sales', header_format)
    worksheet.merge_range('G1:H1', '4W', merge_format)
    worksheet.write(f'G2', 'Platform Sales', header_format)
    worksheet.write(f'H2', '1P Sales', header_format)
    worksheet.write(f'I2', '3P Sales', header_format)
    worksheet.merge_range('J1:L1', '13W', merge_format)
    worksheet.write(f'J2', 'Platform Sales', header_format)
    worksheet.write(f'K2', '1P Sales', header_format)
    worksheet.write(f'L2', '3P Sales', header_format)
    worksheet.merge_range('M1:O1', '26W', merge_format)
    worksheet.write(f'M2', 'Platform Sales', header_format)
    worksheet.write(f'N2', '1P Sales', header_format)
    worksheet.write(f'O2', '3P Sales', header_format)
    worksheet.merge_range('P1:R1', '52W', merge_format)
    worksheet.write(f'P2', 'Platform Sales', header_format)
    worksheet.write(f'Q2', '1P Sales', header_format)
    worksheet.write(f'R2', '3P Sales', header_format)
    worksheet.merge_range('S1:U1', 'YTD', merge_format)
    worksheet.write(f'S2', 'Platform Sales', header_format)
    worksheet.write(f'T2', '1P Sales', header_format)
    worksheet.write(f'U2', '3P Sales', header_format)
    worksheet.set_column('D:U', 11, currency_format)
    worksheet.set_column('C:C', 11, currency_format)
    worksheet.set_column('B:B', 13, currency_format)
    worksheet.set_column('A:A', 27, currency_format)

    print(f'Done writing to ASIN Detail')


def main():
    parser = argparse.ArgumentParser(description='Brand party report')
    parser.add_argument('--beaconClientId', type=int, dest='beacon_client_id', help='beacon_client_id', required=True)
    parser.add_argument('--endWeekId', type=int, dest='end_week_id', help='latest weekid to run report for', required=True)

    args = parser.parse_args()
    print(args)
    max_week = args.end_week_id
    min_week = ((int(max_week / 100) - 1) * 100) + max_week % 10  # go back one year.
    beacon_client = args.beacon_client_id

    brands_to_division = get_client_division_data(beacon_client)
    for index, row in brands_to_division.iterrows():
        division_name = row['divisionname']
        company_name = row['companyname']
        brand_ids = __strlist_to_list__(row['atlasbrandsowned'])
        cat_ids = __strlist_to_list__(row['atlascategoriessubscribed'])
        retailer_ids = __strlist_to_list__(row['atlasretailerssubscribed'])
        print(f'Running for company:{company_name} division: {division_name} '
              f'retailers: {retailer_ids} brands: {brand_ids}')

        if retailer_ids != ['1']:
            print(f'Skipping bad retailers, only running for amzn us for now.')
            continue

        sku_metadata = read_retailerproducts(brand_ids, retailer_ids=retailer_ids)
        print(f'Found {len(sku_metadata)} skus in the brands')

        atlas_data = get_atlas_data_athena(brand_ids, cat_ids, sku_metadata, min_week, max_week)
        print(f'Found {len(atlas_data)} rows in atlas')

        beacon_data = get_beacon_data(brand_ids, beacon_client, sku_metadata, min_week)
        print(f'Found {len(beacon_data)} skus in beacon')

        atlas_join = atlas_data.set_index(['retailersku', 'weekid'])
        beacon_join = beacon_data \
            .rename(columns={'retailSales': '1P Sales', 'weekId': 'weekid'}) \
            .set_index(['retailersku', 'weekid'])

        joint = atlas_join.join(beacon_join, how='outer', lsuffix='_atlas', rsuffix='_beacon').reset_index()
        joint['Brand'] = joint['Brand_atlas'].combine_first(joint['Brand_beacon'])
        joint = joint.round(2)
        print(f'Rows in joint: {len(joint)}')

        sku_rollup = rollup_sku_level(joint, max_week)
        print(f'Rows in sku_rollup: {len(sku_rollup)}')

        brand_rollup = rollup_brand_level(joint, max_week)
        print(f'Rows in brand rollup {len(brand_rollup)}')

        dest_path = f'reports/1p3pBrandReport/1P3PBrandReport_{max_week}_{beacon_client}_{division_name}.xlsx'.replace(" ", "_")
        file_path = 'output.xlsx'
        options = {}
        options['strings_to_formulas'] = False
        options['strings_to_urls'] = False
        print(f'Writing to file_path {file_path}')

        with pd.ExcelWriter(file_path, options=options) as writer:
            write_asin_level(sku_rollup, writer)
            print(f'Done writing to ASIN Detail')
            write_brand_level(brand_rollup, writer)

        s3 = boto3.resource('s3')
        print(f'Copying from local {file_path} to dest: {dest_path}')
        s3.meta.client.upload_file(file_path, 'sl-insights-data', dest_path)



if __name__ == '__main__':
    main()
