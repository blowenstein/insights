from data_collection_utils import get_client_division_data, __strlist_to_list__
import boto3
import pandas as pd
import s3fs
import functools
import pandas.api.types as ptypes
from athena_utils import query_athena
import argparse

fs = s3fs.core.S3FileSystem()
s3 = boto3.client('s3')
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


def main():
    parser = argparse.ArgumentParser(description='Data Dashboard Report')
    parser.add_argument('--startWeekId', type=int, dest='start_week_id', help='first weekid to run report for',
                        required=True)
    parser.add_argument('--endWeekId', type=int, dest='end_week_id', help='latest weekid to run report for',
                        required=True)
    parser.add_argument('--input', type=str, dest='input_path', help='path to file containing input_path skus',
                        required=True)

    args = parser.parse_args()
    print(args)
    min_week = args.start_week_id
    max_week = args.end_week_id
    input_path = args.input_path

    input_skus = pd.read_excel(input_path)

    assert input_skus.columns == ['asin'] and ptypes.is_string_dtype(input_skus['asin'])
    skus_str = ','.join(list([f'\'{x}\'' for x in input_skus['asin']]))
    sales_query = f'''
        select retailersku, weekid, volume as unitssold, retailprice, estimatesales as RetailSales
        from atlas.atlasproductsales
        where
            retailersku in ( {skus_str} )
            and retailerid = 1 
            and weekid >= {min_week}
            and weekid <= {max_week}
    '''
    traffic_query = f'''
        select retailersku, weekid, sum(organicclicks) as organicTraffic, sum(adclicks) as paidTraffic,
               sum(otherClicks) as OtherTraffic, sum(adSpend) as adSpend, sum(adRetailSales) as AdSales
        from atlas.atlastraffic_20191211
        where retailerid = 1
            and retailersku in ( {skus_str} )
            and retailerid = 1 
            and weekid >= {min_week}
            and weekid <= {max_week}
        group by retailersku, weekid
    '''

    sales_df = query_athena(sales_query, 'atlas') \
        .rename(columns={
        'weekid': 'WeekId',
        'retailersku': 'RetailerSku',
        'unitssold': 'UnitSold',
        'retailprice': 'RetailPrice'
    }) \
        .set_index(['WeekId', 'RetailerSku'])

    traffic_df = query_athena(traffic_query, 'atlas') \
        .rename(columns={
        'weekid': 'WeekId',
        'retailersku': 'RetailerSku',
        'organicTraffic': 'Organic Traffic',
        'paidTraffic': 'Paid Traffic',
        'OtherTraffic': 'Other Traffic',
        'adSpend': 'AdSpend'
    }) \
        .set_index(['WeekId', 'RetailerSku']) \
        .round(0)

    joint = sales_df.join(traffic_df, how='outer').reset_index()

    final_df = joint[
        ['WeekId', 'RetailerSku', 'UnitSold', 'RetailPrice', 'RetailSales', 'Organic Traffic', 'Paid Traffic',
         'Other Traffic', 'AdSpend', 'AdSales']].fillna(0)

    dest_path = f'reports/DataDashboard/' \
        f'sony_{min_week}_{max_week}/Stackline_AmazonDashboardData.xlsx'
    file_path = f'output.xlsx'
    options = {
        'strings_to_formulas': False,
        'strings_to_urls': False
    }
    print(f'Writing to file_path {file_path} with options: {options}')

    with pd.ExcelWriter(file_path, options=options) as writer:
        final_df.to_excel(writer, index=False, sheet_name='Data Dashboard')
        workbook = writer.book
        worksheet = writer.sheets['Data Dashboard']
        currency_format = workbook.add_format({'num_format': '[$$-409]#,##0.00'})
        worksheet.set_column('D:E', 11, currency_format)
        worksheet.set_column('J:J', 11, currency_format)

    s3 = boto3.resource('s3')
    print(f'Copying from local {file_path} to dest: {dest_path}')
    s3.meta.client.upload_file(file_path, 'sl-insights-data', dest_path)


if __name__ == '__main__':
    main()
