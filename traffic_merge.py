from parquet_utils import read_parquet
import pandas as pd

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)



metric_rollup_path = 's3://shopper-behavior/metric-rollup/output/computeid=1.0.0/retailerid=11/weekid=201901/'
metric_rollup_df = read_parquet(metric_rollup_path)
metric_rollup_df['retailerid'] = 11
metric_rollup_df['weekid'] = 201901

# print(metric_rollup_df.dtypes)
# grouped_count = metric_rollup_df.groupby(['searchterm', 'retailersku']).count().sort_values(by=['adspend'])
# print(f'GroupedCount: {grouped_count}')

filtered = metric_rollup_df[(metric_rollup_df['retailersku'] == 'B079GWB3KZ') & (metric_rollup_df['searchterm'] == 'babyliss flat iron')]
print(filtered)

other_traffic_path = 's3://shopper-behavior/other-traffic/output/atlas/prod/retailerid=11/weekid=201901/'
other_traffic_df = read_parquet(other_traffic_path)
#filtered = other_traffic_df[other_traffic_df['retailersku'] == 'B079GWB3KZ' and other_traffic_df['searchterm'] == 'babyliss flat iron']

metric_rollup_df = metric_rollup_df[['retailerid', 'weekid', 'retailersku', 'searchterm', 'brandid', 'brandname']]
